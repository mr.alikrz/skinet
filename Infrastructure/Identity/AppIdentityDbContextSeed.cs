﻿using Core.Entities.Identity;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Identity;

public class AppIdentityDbContextSeed
{
    public static async Task SeedUserAsync(UserManager<AppUser> userManager)
    {
        if (!userManager.Users.Any())
        {
            var user = new AppUser()
            {
                DisplayName = "Ali",
                Email = "mr.alikrz@gmail.com",
                UserName = "mr.alikrz@gmail.com",
                Address = new Address
                {
                    FirstName = "Ali",
                    LastName = "Keshavarz",
                    Street = "Afarinesh",
                    City = "Shiraz",
                    State = "Shiraz",
                    ZipCode = "123456"
                }
            };
            await userManager.CreateAsync(user, "Pa$$w0rd");
        }
    }
}